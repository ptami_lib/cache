package cache

import (
	"github.com/bradfitz/gomemcache/memcache"
)

type Client struct {
	mc *memcache.Client
}

func New(url string) *Client {
	return &Client{
		mc: memcache.New(url),
	}
}

func (c Client) Write(key string, value []byte) (err error) {
	err = c.mc.Set(&memcache.Item{Key: key, Value: value})

	return
}

func (c Client) Read(key string, defaultValue []byte) (value []byte, err error) {
	var item *memcache.Item

	item, err = c.mc.Get(key)
	if nil != err {
		value = defaultValue
		return
	}

	value = item.Value

	return
}
